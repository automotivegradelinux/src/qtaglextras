QT += aglextras qml quick

SOURCES += \
    main.cpp

RESOURCES += \
    resources.qrc

target.path = $$[QT_INSTALL_EXAMPLES]/aglextras/helloworld
INSTALLS += target
